package ru.nsu.g.karavaev.g.calculator;

import ru.nsu.g.karavaev.g.calculator.operations.Operation;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Properties;


public class OperationFactory {
    private HashMap<String, Operation> operations;
    private Properties properties;
    OperationFactory() {
        operations = new HashMap<>();
    }

    //operations file parser
    public void scanConfig(String filename) {
        try (InputStream in = OperationFactory.class.getResourceAsStream(filename)) {
            this.properties = new Properties();
            properties.load(in);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        /*
        Scanner scanner = new Scanner(OperationFactory.class.getResourceAsStream(filename));
        while (scanner.hasNext()) {
            String operationName = scanner.next();
            String operationClassName = scanner.next();
            try {
                operations.put(operationName,
                        (Operation) Class
                                .forName("ru.nsu.g.karavaev.g.calculator.operations." + operationClassName)
                                .getDeclaredConstructor()
                                .newInstance());
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }*/
    }

    public void putOperation(String opName, Operation operation) {
        operations.put(opName, operation);
    }

    public Operation getOperation(String opName) {
        if (operations.get(opName) != null)
            return operations.get(opName); // throw new IllegalArgumentException("No such operation");
        else  {
            String desOp;
            if (opName.length() > 1)
                desOp = opName.substring(0, 1).toUpperCase() + opName.substring(1);
            else {
                desOp = this.properties.getProperty(opName);
                if (desOp == null) throw new IllegalArgumentException("No such property");
            }
            try {
                operations.put(opName, (Operation) Class
                        .forName("ru.nsu.g.karavaev.g.calculator.operations." + desOp)
                        .getDeclaredConstructor()
                        .newInstance());
            } catch (Exception e) {
                throw new IllegalArgumentException("No such property");
            }
            return operations.get(opName);
        }
    }
}