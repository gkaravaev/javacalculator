package ru.nsu.g.karavaev.g.calculator;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Scanner;

public class Parser {
    private InputStream in;
    private ArrayList<String> opList;

    Parser(){
    }

    Parser(InputStream is, ArrayList<String> ops) {
        this.in = is;
        this.opList = ops;
    }

    public void setInputStream (InputStream in){
        this.in = in;
    }

    public void setOpList (ArrayList<String> opList) {
        this.opList = opList;
    }

    public void scan() {
        Scanner scanner = new Scanner(in);
        while (scanner.hasNext())
            opList.add(scanner.next());
    }
}
