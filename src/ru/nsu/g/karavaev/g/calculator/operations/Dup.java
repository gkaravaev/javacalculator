package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Dup implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        if (stack.peek() != null) {
            stack.addLast(stack.getLast());
        } else throw new NullPointerException();
    }
}