package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Divide implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        Integer a = stack.pollLast();
        Integer b = stack.pollLast();
        if (a == null || b == null) throw new NullPointerException();
        if (b == 0) throw new ArithmeticException();
        stack.addLast(b / a);
    }
}
