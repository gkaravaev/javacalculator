package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Rot implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        Integer a = stack.pollLast();
        Integer b = stack.pollLast();
        Integer c = stack.pollLast();
        if (a == null || b == null || c == null) throw new NullPointerException();
        stack.addLast(b);
        stack.addLast(a);
        stack.addLast(c);
    }
}