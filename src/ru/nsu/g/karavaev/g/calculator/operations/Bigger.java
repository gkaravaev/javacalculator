package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Bigger implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        Integer a = stack.pollLast();
        Integer b = stack.pollLast();
        if (a == null || b == null) throw new NullPointerException();
        if (a > b) {
            stack.add(1);
        } else {
            stack.add(0);
        }
    }
}
