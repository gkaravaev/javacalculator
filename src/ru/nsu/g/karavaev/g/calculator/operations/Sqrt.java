package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Sqrt implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        Integer a = stack.pollLast();
        if (a == null) throw new NullPointerException();
        if (a < 0) throw new ArithmeticException();
        stack.addLast((int)Math.floor(Math.sqrt((double) a)));
    }
}