package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Print implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        System.out.println(stack.peekLast());
    }
}