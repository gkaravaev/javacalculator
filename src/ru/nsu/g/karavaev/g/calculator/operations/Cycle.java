package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class Cycle implements Operation {
    private ArrayList<Operation> loopOps;

    public Cycle(ArrayList<Operation> loopOps) {
        this.loopOps = loopOps;
    }
    public void doOperation(ArrayDeque<Integer> stack) {
        while (stack.peekLast() != 0) {
            stack.pollLast();
            for (Operation op : loopOps) {
                op.doOperation(stack);
            }
        }
        Drop drop = new Drop();
        drop.doOperation(stack);
    }
}
