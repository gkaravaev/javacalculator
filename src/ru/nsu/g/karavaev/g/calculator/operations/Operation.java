package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public interface Operation {
    public void doOperation(ArrayDeque<Integer> stack);
}
