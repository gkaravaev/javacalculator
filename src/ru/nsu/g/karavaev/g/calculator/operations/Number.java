package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Number implements Operation{

    private Integer number;

    public Number(Integer number) {
        this.number = number;
    }

    public void doOperation(ArrayDeque<Integer> stack) {
        stack.addLast(number);
    }
}
