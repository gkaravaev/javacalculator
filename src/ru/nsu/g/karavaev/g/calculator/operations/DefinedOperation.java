package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;
import java.util.ArrayList;

public class DefinedOperation implements Operation {
    private String name;
    ArrayList<Operation> ops;

    public DefinedOperation(String name, ArrayList<Operation> ops) {
        this.name = name;
        this.ops = ops;
    }
    public void doOperation(ArrayDeque<Integer> stack) {
        for (Operation op : ops) {
            op.doOperation(stack);
        }
    }
}
