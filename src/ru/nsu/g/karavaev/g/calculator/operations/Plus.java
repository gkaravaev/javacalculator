package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Plus implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        Integer a = stack.pollLast();
        Integer b = stack.pollLast();
        if (a == null || b == null) throw new NullPointerException();
        stack.addLast(a + b);
    }
}
