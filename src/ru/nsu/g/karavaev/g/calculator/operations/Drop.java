package ru.nsu.g.karavaev.g.calculator.operations;

import java.util.ArrayDeque;

public class Drop implements Operation {
    public void doOperation(ArrayDeque<Integer> stack) {
        stack.pollLast();
    }
}
