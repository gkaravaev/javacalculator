package ru.nsu.g.karavaev.g.calculator;

import ru.nsu.g.karavaev.g.calculator.operations.Cycle;
import ru.nsu.g.karavaev.g.calculator.operations.DefinedOperation;
import ru.nsu.g.karavaev.g.calculator.operations.Number;
import ru.nsu.g.karavaev.g.calculator.operations.Operation;

import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;

@SuppressWarnings("Duplicates")
public class Calculator {
    private ArrayDeque<Integer> stack;
    private InputStream inputStream;
    private OperationFactory operations;
    private ArrayList<String> opList;
    private Parser parser;


    Calculator(InputStream stream) {
        this.stack = new ArrayDeque<>();
        this.opList = new ArrayList<>();
        this.inputStream = stream;
        this.operations = new OperationFactory();
        operations.scanConfig("operations.txt");
        this.parser = new Parser(inputStream, opList);
    }

    private void parseDefine(MutableInt currentPosition) {
        currentPosition.value++;
        String opName = opList.get(currentPosition.value);
        currentPosition.value++;
        ArrayList<Operation> defOps = new ArrayList<>();
        while (!opList.get(currentPosition.value).equals(";")) {
            if (opList.get(currentPosition.value).chars().allMatch(Character::isDigit)) {
                defOps.add(new Number(Integer.parseInt(opList.get(currentPosition.value))));
            } else if (opList.get(currentPosition.value).equals("[")) {
                defOps.add(parseCycle(currentPosition));
            } else {
                try {
                    defOps.add(operations.getOperation(opList.get(currentPosition.value)));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            currentPosition.value++;
        }
        operations.putOperation(opName, new DefinedOperation(opName, defOps));
    }

    private Cycle parseCycle(MutableInt currentPosition) {
        ArrayList<Operation> defOps = new ArrayList<>();
        currentPosition.value++;
        while (!opList.get(currentPosition.value).equals("]")) {
            if (opList.get(currentPosition.value).equals("[")) {
                defOps.add(parseCycle(currentPosition));
            } else if (opList.get(currentPosition.value).chars().allMatch(Character::isDigit))
                defOps.add(new Number(Integer.parseInt(opList.get(currentPosition.value))));
            else {
                try {
                    defOps.add(operations.getOperation(opList.get(currentPosition.value)));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
            currentPosition.value++;
        }
        return new Cycle(defOps);
    }

    public void interpretate() {
        parser.scan();
        for (MutableInt i = new MutableInt(0); i.value < opList.size(); i.value++) {
            if (opList.get(i.value).chars().allMatch(Character::isDigit))
                stack.addLast(Integer.parseInt(opList.get(i.value)));
            else if (opList.get(i.value).equals("define")) {
                parseDefine(i);
            } else if (opList.get(i.value).equals("[")) {
                parseCycle(i).doOperation(stack);
            } else {
                try {
                    operations.getOperation(opList.get(i.value)).doOperation(stack);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

}
