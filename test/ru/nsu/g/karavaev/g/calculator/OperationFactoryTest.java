package ru.nsu.g.karavaev.g.calculator;

import org.junit.Test;
import ru.nsu.g.karavaev.g.calculator.operations.DefinedOperation;
import ru.nsu.g.karavaev.g.calculator.operations.Operation;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class OperationFactoryTest {
    private final OperationFactory operationFactory = new OperationFactory();

    @Test
    public void opFactoryTest (){
        DefinedOperation testOp = new DefinedOperation("newOp", new ArrayList<>());
        operationFactory.putOperation("newOp", testOp);
        assertEquals(operationFactory.getOperation("newOp"), testOp);
    }
}