package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class DupTest {
    @Test
    public void test() {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(1);
        stack.addLast(2);
        Dup dup = new Dup();
        dup.doOperation(stack);
        assertEquals((int)stack.pollLast(), 2);
        assertEquals((int)stack.peekLast(), 2);

    }
}