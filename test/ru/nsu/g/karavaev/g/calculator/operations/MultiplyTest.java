package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class MultiplyTest {
    @Test
    public void test() {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(2);
        stack.addLast(5);
        Multiply multiply = new Multiply();
        multiply.doOperation(stack);
        assertEquals((int)stack.peekLast(), 10);
    }

}