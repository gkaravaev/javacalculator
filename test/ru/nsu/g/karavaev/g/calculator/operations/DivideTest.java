package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class DivideTest {
    @Test
    public void test() {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(10);
        stack.addLast(5);
        Divide divide = new Divide();
        divide.doOperation(stack);
        assertEquals((int)stack.peekLast(), 2);
    }
}