package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class SqrtTest {
    @Test
    public void test() {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(9);
        Sqrt sqrt = new Sqrt();
        sqrt.doOperation(stack);
        assertEquals((int)stack.peekLast(), 3);
    }

}