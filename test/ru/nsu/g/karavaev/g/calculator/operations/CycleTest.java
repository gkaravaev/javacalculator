package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class CycleTest {
    @Test
    public void test() {
        ArrayList<Operation> loopOps = new ArrayList<>();
        loopOps.add(new Dup());
        loopOps.add(new Rot());
        loopOps.add(new Multiply());
        loopOps.add(new Swap());
        loopOps.add(new Number(1));
        loopOps.add(new Minus());
        loopOps.add(new Dup());
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(1);
        stack.addLast(5);
        stack.addLast(5);
        Cycle cycle = new Cycle(loopOps);
        cycle.doOperation(stack);
        stack.pollLast();
        assertEquals((int)stack.peekLast(), 120);
    }
}