package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class PlusTest {
    @Test
    public void test() {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(1);
        stack.addLast(2);
        Plus plus = new Plus();
        plus.doOperation(stack);
        assertEquals((int)stack.pollLast(), 3);
    }

}