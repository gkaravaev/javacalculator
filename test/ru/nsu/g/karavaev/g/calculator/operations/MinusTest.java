package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class MinusTest {
    @Test
    public void test() {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(2);
        stack.addLast(1);
        Minus minus = new Minus();
        minus.doOperation(stack);
        assertEquals((int)stack.peekLast(), 1);
    }

}