package ru.nsu.g.karavaev.g.calculator.operations;

import org.junit.Test;

import java.util.ArrayDeque;

import static org.junit.Assert.*;

public class BiggerTest {
    @Test
    public void test() {
        ArrayDeque<Integer> stack = new ArrayDeque<>();
        stack.addLast(1);
        stack.addLast(2);
        Bigger bigger = new Bigger();
        bigger.doOperation(stack);
        assertEquals((int) stack.peekLast(), 1);
    }

}