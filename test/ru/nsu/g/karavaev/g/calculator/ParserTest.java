package ru.nsu.g.karavaev.g.calculator;

import org.junit.Test;
import ru.nsu.g.karavaev.g.calculator.operations.Operation;


import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.*;

public class ParserTest {
    private final Parser parser = new Parser();

    @Test
    public void scanTest(){
        ArrayList<String> opList = new ArrayList<>();
        ByteArrayInputStream bais = new ByteArrayInputStream("+ - 1 2 print".getBytes());
        parser.setInputStream(bais);
        parser.setOpList(opList);
        parser.scan();
        ArrayList<String> realList = new ArrayList<String>();
        realList.add("+");
        realList.add("-");
        realList.add("1");
        realList.add("2");
        realList.add("print");
        assertEquals(opList, realList);

    }
}